# The last minecraft server you'll ever need

A FOSS minecraft server based on the the popular fork paper MC that takes care of the hard bits

because managing, updating, deploying a server is hard!

## Getting Started

### Running the server with Docker (recommend):

dependencies that must be installed before continuing:
 - docker
 - docker-compose
 - bash

you can either just use docker-compose:
```bash
docker-compose up
```

or if you want an interactive console, use the run script
```bash
bash run
```

### Running the server without Docker:

dependencies that must be installed before continuing:
 - [jq](https://stedolan.github.io/jq/)
 - [java](https://www.oracle.com/java/technologies/downloads/)
 - bash
 - curl

you can run the minecraft server on bare-metal with the run command:
```bash
bash run metal
```

or if you prefer to run the script manualy:
```bash
cd server
bash server.sh
```

### Restoring game.jesseg.co Server Backups (WIP):

- Download the latest world/plugin backups:
  ```
  https://drive.google.com/drive/folders/1cq_lWQ0THpnXm58iu0VHufKwd3h5N0Wf?usp=sharing
  ```
- Extract the zip files in the server directory

It's not ideal for the moment, but at least it's backed up

### Updating plugins using pluGET (experimental):

you MUST have at least docker to run this, but it uses a little program called pluGET.

I've looked at a lot of alternatives and couldn't find anything that wouldn't depend on
a premium/external service that just feels hacky (I'm looking at you autoplug!)

to manage plugins using the application, just run:
```bash
bash run pluget
```

or to just update all the plugins, run:
```bash
bash run pluget update
```

to further configure pluGET, run:
```bash
bash run pluget config
```
